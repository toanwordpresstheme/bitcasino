var swiper = new Swiper('.slide-trending.swiper-container', {
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    // spaceBetween:16,
    slidesPerView: 6,
    breakpoints: {
      // when window width is >= 320px
      768: {
        slidesPerView: 3,
        spaceBetween:8,
      }
  
    }
   
  });
  var swiper = new Swiper('.provider-slide.swiper-container', {
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    spaceBetween:16,
    slidesPerView:6,
    breakpoints: {
      // when window width is >= 320px
      768: {
        slidesPerView: 2,
       
      }
  
    }

  });
  var swiper = new Swiper('.testimonials-slide.swiper-container', {
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    spaceBetween:16,
    slidesPerView:4,
    breakpoints: {
      // when window width is >= 320px
      768: {
        slidesPerView: 1,
       
      }
  
    }

  });
 
$(document).ready(function (){
  // Trending js
    $(".trending-btn-next").click(function (){
        
        $(this).closest(".trending").find(".slide-trending .swiper-button-next").trigger("click");
    })
    $(".trending-btn-prev").click(function (){
        
        $(this).closest(".trending").find(".slide-trending .swiper-button-prev").trigger("click");
    })

   $(".menu-mobile").click(function (){
      $(".left-content").toggleClass("activemenu");
   })
   $(".mobile-close").click(function(){
      $(".left-content").toggleClass("activemenu");
   })
    
})